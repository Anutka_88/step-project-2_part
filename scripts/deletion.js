async function deletion_card(id) {
    let token = localStorage.getItem("token");
    let request = await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
        method: 'DELETE',
        headers: {
          'Authorization': `Bearer ${token}`
        },
      });
    let backend_cards = JSON.parse(localStorage.getItem("backend_cards"));
    if(request.ok === true && request.status === 200){
        let card_to_del = document.querySelector(`.id${id}`);
        card_to_del.remove();

        let temp_arr = [];
        for(let i = 0; i < backend_cards.length; i++){
          if(backend_cards[i].id === id) {
            temp_arr = backend_cards.splice(i, 1);
          }
        }   
      localStorage.setItem("backend_cards", JSON.stringify(backend_cards));
      localStorage.setItem("test", "test2");
    }
    if (!document.querySelector(".visit")) { 
      document.querySelector(".missing__elements").classList.remove("hidden");
    }
  }
  export default deletion_card;
  
  